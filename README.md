# Created Personal Website using Bootstrap 3.1.1
* https://getbootstrap.com/

## About the author
* Navedanjum Ansari, navedanjum.ansari@gmail.com  
* Linkedin Profile: https://www.linkedin.com/in/navedanjum-a-6018783b/    

## Description

* This website is created as a hands-on experience with HTML, CSS and Basic JavaScript.  
* This website is made responsive and therefore can be viewed in different device size.  
* At the moment, the website is hosted with 000webhost on the domain www.xpressionworks.com   
* Used sublime code editor to write the HTML/CSS and JavaScipt code
* The main aim to create this site is to put the theory learned into practice and understand overall process of building and hosting website.   
* This website is the result of 40 hours bootcamp course to learn building and hosting professional website quickly.   

## Learning Outcomes    
* CORE FUNDAMENTALS : use a text-editor and the fundamentals of HTML and CSS so to get ready to experiment with advance concepts.    
* RAPID DEVELOPMENT : use of powerful frameworks like Bootstrap, Font-awesome and Google Forms so to make a good looking site very quickly.   
* HTML5 & CSS3 :  Fresh and new look for the website.    
* RESPONSIVE DESIGN: responsive design so that the website looks great on all devices (laptop, Ipad, mobile phones).   
* Web hosting and deployment : Learned to quickly host website on personal domain.    












