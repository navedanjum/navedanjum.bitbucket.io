/*
Created this JS for the purpose of learning basic JS functionality
Will enhance as I learn the advance topics
Created: 27/08/2016

*/

	  function formattedDate(date) {
		        var d = new Date(date || Date.now()),
		        month = '' + (d.getMonth() + 1),
		        day = '' + d.getDate(),
		        year = d.getFullYear();

		    if (month.length < 2) month = '0' + month;
		    if (day.length < 2) day = '0' + day;

	        //return [month, day, year].join('/');
	        document.getElementById("container").innerHTML = [day, month, year].join('/') + "</br>";
	        //change();
     }


	   function labeChange() // code to change button label
		{
		    var elem = document.getElementById("myButton1");
		    if (elem.value=="Show Date") {
		    	formattedDate();
		    	elem.value = "Hide Date";
		    }
		    else {
		    	document.getElementById("container").innerHTML ="";
		    	elem.value = "Show Date";
		    }
		}